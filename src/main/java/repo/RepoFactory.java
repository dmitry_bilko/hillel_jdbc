package repo;

public interface RepoFactory<T> {

   <U extends Repo<T>> U getRepo();
}
