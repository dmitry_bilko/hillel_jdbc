package repo;

import java.util.List;

import org.hibernate.Session;

public interface Repo<T> {

    T findOne(final Session session, final int id);

    List<T> findAll(final Session session);

    void updateOne(final Session session, final T model);

    void deleteOne(final Session session, final int id);
}
