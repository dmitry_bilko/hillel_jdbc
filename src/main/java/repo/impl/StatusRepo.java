package repo.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Session;

import model.Status;

import repo.Repo;

import static java.util.Collections.emptyList;

public class StatusRepo implements Repo<Status> {

    @Override
    public Status findOne(final Session session, final int id) {
        if (session == null) return null;
        return session.get(Status.class, id);
    }

    @Override
    public List<Status> findAll(final Session session) {
        if (session == null) return emptyList();
        final CriteriaQuery<Status> criteria = session
            .getCriteriaBuilder()
            .createQuery(Status.class);
        criteria.select(criteria.from(Status.class));
        return session
            .createQuery(criteria)
            .getResultList();
    }

    @Override
    public void updateOne(final Session session, final Status status) {
        if (session == null) return;
        session.saveOrUpdate(status);
    }

    @Override
    public void deleteOne(final Session session, final int id) {
        if (session == null) return;
        final Status status = findOne(session, id);
        if (status != null) session.delete(status);
    }
}
