package repo.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Session;

import model.Task;

import repo.Repo;

import static java.util.Collections.emptyList;

public class TaskRepo implements Repo<Task> {

    @Override
    public Task findOne(final Session session, final int id) {
        if (session == null) return null;
        return session.get(Task.class, id);
    }

    @Override
    public List<Task> findAll(final Session session) {
        if (session == null) return emptyList();
        final CriteriaQuery<Task> criteria = session
            .getCriteriaBuilder()
            .createQuery(Task.class);
        criteria.select(criteria.from(Task.class));
        return session
            .createQuery(criteria)
            .getResultList();
    }

    @Override
    public void updateOne(final Session session, final Task task) {
        if (session == null) return;
        session.saveOrUpdate(task);
    }

    @Override
    public void deleteOne(final Session session, final int id) {
        if (session == null) return;
        final Task task = findOne(session, id);
        if (task != null) session.delete(task);
    }
}
