package repo.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Session;

import model.User;

import repo.Repo;

import static java.util.Collections.emptyList;

public class UserRepo implements Repo<User> {

    @Override
    public User findOne(final Session session, final int id) {
        if (session == null) return null;
        return session.get(User.class, id);
    }

    @Override
    public List<User> findAll(final Session session) {
        if (session == null) return emptyList();
        final CriteriaQuery<User> criteria = session
            .getCriteriaBuilder()
            .createQuery(User.class);
        criteria.select(criteria.from(User.class));
        return session
            .createQuery(criteria)
            .getResultList();
    }

    @Override
    public void updateOne(final Session session, final User user) {
        if (session == null) return;
        session.saveOrUpdate(user);
    }

    @Override
    public void deleteOne(final Session session, final int id) {
        if (session == null) return;
        final User user = findOne(session, id);
        if (user != null) session.delete(user);
    }
}
