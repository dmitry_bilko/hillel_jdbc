package repo.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Session;

import model.Priority;

import repo.Repo;

import static java.util.Collections.emptyList;

public class PriorityRepo implements Repo<Priority> {

    @Override
    public Priority findOne(final Session session, final int id) {
        if (session == null) return null;
        return session.get(Priority.class, id);
    }

    @Override
    public List<Priority> findAll(final Session session) {
        if (session == null) return emptyList();
        final CriteriaQuery<Priority> criteria = session
            .getCriteriaBuilder()
            .createQuery(Priority.class);
        criteria.select(criteria.from(Priority.class));
        return session
            .createQuery(criteria)
            .getResultList();
    }

    @Override
    public void updateOne(final Session session, final Priority priority) {
        if (session == null) return;
        session.saveOrUpdate(priority);
    }

    @Override
    public void deleteOne(final Session session, final int id) {
        if (session == null) return;
        final Priority priority = findOne(session, id);
        if (priority != null) session.delete(priority);
    }
}
