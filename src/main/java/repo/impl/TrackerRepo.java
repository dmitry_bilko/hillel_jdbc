package repo.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Session;

import model.Tracker;

import repo.Repo;

import static java.util.Collections.emptyList;

public class TrackerRepo implements Repo<Tracker> {

    @Override
    public Tracker findOne(final Session session, final int id) {
        if (session == null) return null;
        return session.get(Tracker.class, id);
    }

    @Override
    public List<Tracker> findAll(final Session session) {
        if (session == null) return emptyList();
        final CriteriaQuery<Tracker> criteria = session
            .getCriteriaBuilder()
            .createQuery(Tracker.class);
        criteria.select(criteria.from(Tracker.class));
        return session
            .createQuery(criteria)
            .getResultList();
    }

    @Override
    public void updateOne(final Session session, final Tracker tracker) {
        if (session == null) return;
        session.saveOrUpdate(tracker);
    }

    @Override
    public void deleteOne(final Session session, final int id) {
        if (session == null) return;
        final Tracker tracker = findOne(session, id);
        if (tracker != null) session.delete(tracker);
    }
}
