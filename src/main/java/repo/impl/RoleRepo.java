package repo.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Session;

import model.Role;

import repo.Repo;

import static java.util.Collections.emptyList;

public class RoleRepo implements Repo<Role> {

    @Override
    public Role findOne(final Session session, final int id) {
        if (session == null) return null;
        return session.get(Role.class, id);
    }

    @Override
    public List<Role> findAll(final Session session) {
        if (session == null) return emptyList();
        final CriteriaQuery<Role> criteria = session
            .getCriteriaBuilder()
            .createQuery(Role.class);
        criteria.select(criteria.from(Role.class));
        return session
            .createQuery(criteria)
            .getResultList();
    }

    @Override
    public void updateOne(final Session session, final Role role) {
        if (session == null) return;
        session.saveOrUpdate(role);
    }

    @Override
    public void deleteOne(final Session session, final int id) {
        if (session == null) return;
        final Role role = findOne(session, id);
        if (role != null) session.delete(role);
    }
}
