package repo;

import model.User;

import repo.impl.UserRepo;

@SuppressWarnings("unused")
public class UserRepoFactory implements RepoFactory<User> {

    private static UserRepoFactory instance;

    private UserRepoFactory() { }

    public static synchronized UserRepoFactory getInstance() {
        if (instance == null) instance = new UserRepoFactory();
        return instance;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Repo<User> getRepo() {
        return new UserRepo();
    }
}
