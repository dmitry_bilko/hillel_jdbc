package repo;

import model.Role;

import repo.impl.RoleRepo;

@SuppressWarnings("unused")
public class RoleRepoFactory implements RepoFactory<Role> {

    private static RoleRepoFactory instance;

    private RoleRepoFactory() { }

    public static synchronized RoleRepoFactory getInstance() {
        if (instance == null) instance = new RoleRepoFactory();
        return instance;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Repo<Role> getRepo() {
        return new RoleRepo();
    }
}
