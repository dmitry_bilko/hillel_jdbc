package repo;

import model.Task;

import repo.impl.TaskRepo;

@SuppressWarnings("unused")
public class TaskRepoFactory implements RepoFactory<Task> {

    private static TaskRepoFactory instance;

    private TaskRepoFactory() { }

    public static synchronized TaskRepoFactory getInstance() {
        if (instance == null) instance = new TaskRepoFactory();
        return instance;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Repo<Task> getRepo() {
        return new TaskRepo();
    }
}
