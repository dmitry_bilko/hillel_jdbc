package repo;

import model.Status;

import repo.impl.StatusRepo;

@SuppressWarnings("unused")
public class StatusRepoFactory implements RepoFactory<Status> {

    private static StatusRepoFactory instance;

    private StatusRepoFactory() { }

    public static synchronized StatusRepoFactory getInstance() {
        if (instance == null) instance = new StatusRepoFactory();
        return instance;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Repo<Status> getRepo() {
        return new StatusRepo();
    }
}
