package repo;

import model.Priority;

import repo.impl.PriorityRepo;

@SuppressWarnings("unused")
public class PriorityRepoFactory implements RepoFactory<Priority> {

    private static PriorityRepoFactory instance;

    private PriorityRepoFactory() { }

    public static synchronized PriorityRepoFactory getInstance() {
        if (instance == null) instance = new PriorityRepoFactory();
        return instance;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Repo<Priority> getRepo() {
        return new PriorityRepo();
    }
}
