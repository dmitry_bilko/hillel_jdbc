package repo;

import model.Tracker;

import repo.impl.TrackerRepo;

@SuppressWarnings("unused")
public class TrackerRepoFactory implements RepoFactory<Tracker> {

    private static TrackerRepoFactory instance;

    private TrackerRepoFactory() { }

    public static synchronized TrackerRepoFactory getInstance() {
        if (instance == null) instance = new TrackerRepoFactory();
        return instance;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Repo<Tracker> getRepo() {
        return new TrackerRepo();
    }
}
