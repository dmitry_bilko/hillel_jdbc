package repo.config;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import model.Priority;
import model.Role;
import model.Status;
import model.Task;
import model.Tracker;
import model.User;

public class HibernateUtil {

    private static SessionFactory sessionFactory;

    private HibernateUtil() { }

    public static synchronized SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            final Configuration config = new Configuration()
                .addAnnotatedClass(Priority.class)
                .addAnnotatedClass(Role.class)
                .addAnnotatedClass(Status.class)
                .addAnnotatedClass(Task.class)
                .addAnnotatedClass(Tracker.class)
                .addAnnotatedClass(User.class)
                .configure();
            sessionFactory = config.buildSessionFactory(
                new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build());
        }
        return sessionFactory;
    }
}
