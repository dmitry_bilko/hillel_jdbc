import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import model.User;

import service.UserServiceProvider;

import static java.lang.String.format;

import static repo.config.HibernateUtil.getSessionFactory;

public class Main {

    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(final String[] args) {
        try (final SessionFactory factory = getSessionFactory();
            final Session session = factory.openSession()) {
            final List<User> users = UserServiceProvider
                .getInstance()
                .getUserService(session)
                .getMostProductiveUsers(1);

            if (users == null || users.size() == 0) {
                LOG.warn("Unable to find out the most productive user");
                return;
            }

            final User beast = users.get(0);
            LOG.info(format("%s %s rules!", beast.getFirstName(), beast.getLastName()));
        }
    }
}
