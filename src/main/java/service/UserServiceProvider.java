package service;

import org.hibernate.Session;

import service.impl.UserServiceImpl;

public class UserServiceProvider {

    private static UserServiceProvider instance;

    private UserServiceProvider() { }

    public static synchronized UserServiceProvider getInstance() {
        if (instance == null) instance = new UserServiceProvider();
        return instance;
    }

    public UserService getUserService(final Session session) {
        return new UserServiceImpl(session);
    }
}
