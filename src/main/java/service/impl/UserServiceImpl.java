package service.impl;

import java.util.List;
import java.util.Map.Entry;

import org.hibernate.Session;
import org.hibernate.Transaction;

import model.Task;
import model.User;

import repo.Repo;
import repo.TaskRepoFactory;
import repo.UserRepoFactory;

import service.UserService;

import static java.util.Comparator.reverseOrder;
import static java.util.Map.Entry.comparingByValue;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

import static model.Status.StatusName.RESOLVED;

public class UserServiceImpl implements UserService {

    private Session session;

    public UserServiceImpl(final Session session) {
        this.session = session;
    }

    @Override
    public List<User> getMostProductiveUsers(final int number) {
        final Transaction transaction = session.beginTransaction();
        final List<User> users = TaskRepoFactory
            .getInstance()
            .getRepo()
            .findAll(session)
            .stream()
            .filter(task -> task
                .getStatus()
                .getStatusName()
                .toString()
                .equals(RESOLVED.toString()))
            .collect(groupingBy(Task::getUser, counting()))
            .entrySet()
            .stream()
            .sorted(comparingByValue(reverseOrder()))
            .limit(number)
            .map(Entry::getKey)
            .collect(toList());
        transaction.commit();
        return users;
    }

    @Override
    public void deleteUser(final int id) {
        final Transaction transaction = session.beginTransaction();
        final Repo<Task> taskRepo = TaskRepoFactory
            .getInstance()
            .getRepo();

        taskRepo
            .findAll(session)
            .stream()
            .filter(task -> task.getUser().getUserId() == id)
            .forEach(task -> {
                task.setUser(null);
                taskRepo.updateOne(session, task);
            });

        UserRepoFactory
            .getInstance()
            .getRepo()
            .deleteOne(session, id);
        transaction.commit();
    }
}
