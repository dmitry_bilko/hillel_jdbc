package service;

import java.util.List;

import model.User;

public interface UserService {

    List<User> getMostProductiveUsers(final int number);

    @SuppressWarnings("unused") void deleteUser(final int id);
}
