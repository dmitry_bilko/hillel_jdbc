package model;

import javax.persistence.AttributeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import model.Role.RoleName;

import static org.apache.commons.lang3.StringUtils.isBlank;

public class RoleNameConverter implements AttributeConverter<RoleName, String> {

    private static final Logger LOG = LoggerFactory.getLogger(RoleNameConverter.class);

    @Override
    public String convertToDatabaseColumn(final RoleName role) {
        if (role == null) {
            LOG.warn("Given RoleName is null");
            return null;
        }
        return role.toString();
    }

    @Override
    public RoleName convertToEntityAttribute(final String name) {
        if (isBlank(name)) {
            LOG.warn("Given RoleName value is blank");
            return null;
        }
        return RoleName.value(name);
    }
}
