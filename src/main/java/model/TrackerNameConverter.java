package model;

import javax.persistence.AttributeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import model.Tracker.TrackerName;

import static org.apache.commons.lang3.StringUtils.isBlank;

public class TrackerNameConverter implements AttributeConverter<TrackerName, String> {

    private static final Logger LOG = LoggerFactory.getLogger(TrackerNameConverter.class);

    @Override
    public String convertToDatabaseColumn(final TrackerName tracker) {
        if (tracker == null) {
            LOG.warn("Given TrackerName is null");
            return null;
        }
        return tracker.toString();
    }

    @Override
    public TrackerName convertToEntityAttribute(final String name) {
        if (isBlank(name)) {
            LOG.warn("Given TrackerName value is blank");
            return null;
        }
        return TrackerName.value(name);
    }
}
