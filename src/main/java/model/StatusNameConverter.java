package model;

import javax.persistence.AttributeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import model.Status.StatusName;

import static org.apache.commons.lang3.StringUtils.isBlank;

public class StatusNameConverter implements AttributeConverter<StatusName, String> {

    private static final Logger LOG = LoggerFactory.getLogger(StatusNameConverter.class);

    @Override
    public String convertToDatabaseColumn(final StatusName status) {
        if (status == null) {
            LOG.warn("Given StatusName is null");
            return null;
        }
        return status.toString();
    }

    @Override
    public StatusName convertToEntityAttribute(final String name) {
        if (isBlank(name)) {
            LOG.warn("Given StatusName value is blank");
            return null;
        }
        return StatusName.value(name);
    }
}
