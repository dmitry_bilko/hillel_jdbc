package model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import static java.util.Objects.hash;

@Entity
@Table
@SuppressWarnings({ "WeakerAccess", "unused" })
public class Task {

    private static final String STATUS_FK = "status_id";
    private static final String USER_FK = "user_id";
    private static final String TRACKER_FK = "tracker_id";
    private static final String PRIORITY_FK = "priority_id";

    @Id
    private int task_id;

    @JoinColumn(name = STATUS_FK)
    @ManyToOne
    private Status status;

    @Column
    private String subject;

    @Column
    private String description;

    @JoinColumn(name = USER_FK)
    @ManyToOne
    private User user;

    @JoinColumn(name = TRACKER_FK)
    @ManyToOne
    private Tracker tracker;

    @JoinColumn(name = PRIORITY_FK)
    @ManyToOne
    private Priority priority;

    public Task() { }

    public int getTaskId() {
        return task_id;
    }

    public void setTaskId(final int task_id) {
        this.task_id = task_id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(final String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }

    public void setUser(final User user) {
        this.user = user;
    }

    public Tracker getTracker() {
        return tracker;
    }

    public void setTracker(final Tracker tracker) {
        this.tracker = tracker;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(final Priority priority) {
        this.priority = priority;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Task)) return false;
        final Task task = (Task) o;
        return task_id == task.getTaskId()
            && Objects.equals(status, task.getStatus())
            && Objects.equals(subject, task.getSubject())
            && Objects.equals(description, task.getDescription())
            && Objects.equals(user, task.getUser())
            && Objects.equals(tracker, task.getTracker())
            && Objects.equals(priority, task.getPriority());
    }

    @Override
    public int hashCode() {
        return hash(task_id, status, subject, description, user, tracker, priority);
    }
}
