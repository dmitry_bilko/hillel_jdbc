package model;

import java.util.Objects;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import static java.util.Objects.hash;

import static org.apache.commons.lang3.StringUtils.isBlank;

@Entity
@Table
@SuppressWarnings({ "WeakerAccess", "unused" })
public class Status {

    @Id
    private int status_id;

    @Convert(converter = StatusNameConverter.class)
    private StatusName status_name;

    public Status() { }

    public int getStatusId() {
        return status_id;
    }

    public void setStatusId(final int status_id) {
        this.status_id = status_id;
    }

    public StatusName getStatusName() {
        return status_name;
    }

    public void setStatusName(final StatusName status_name) {
        this.status_name = status_name;
    }

    public enum StatusName {

        NEW("New"),
        FRESH("Fresh"),
        IN_PROGRESS("In Progress"),
        RESOLVED("Resolved");

        private String name;

        StatusName(final String name) {
            this.name = name;
        }

        public static StatusName value(final String name) {
            if (isBlank(name)) return null;
            for (final StatusName value : values()) if (value.name.equals(name)) return value;
            return null;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Status)) return false;
        final Status status = (Status) o;
        return status_id == status.getStatusId()
            && Objects.equals(status_name, status.getStatusName());
    }

    @Override
    public int hashCode() {
        return hash(status_id, status_name);
    }
}
