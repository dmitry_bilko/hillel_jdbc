package model;

import javax.persistence.AttributeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import model.Priority.PriorityName;

import static org.apache.commons.lang3.StringUtils.isBlank;

public class PriorityNameConverter implements AttributeConverter<PriorityName, String> {

    private static final Logger LOG = LoggerFactory.getLogger(PriorityNameConverter.class);

    public String convertToDatabaseColumn(final PriorityName priority) {
        if (priority == null) {
            LOG.warn("Given PriorityName is null");
            return null;
        }
        return priority.toString();
    }

    public PriorityName convertToEntityAttribute(final String name) {
        if (isBlank(name)) {
            LOG.warn("Given PriorityName value is blank");
            return null;
        }
        return PriorityName.value(name);
    }
}
