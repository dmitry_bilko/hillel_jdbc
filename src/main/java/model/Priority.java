package model;

import java.util.Objects;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import static java.util.Objects.hash;

import static org.apache.commons.lang3.StringUtils.isBlank;

@Entity
@Table
@SuppressWarnings({ "WeakerAccess", "unused" })
public class Priority {

    @Id
    private int priority_id;

    @Convert(converter = PriorityNameConverter.class)
    private PriorityName priority_name;

    public Priority() { }

    public int getPriorityId() {
        return priority_id;
    }

    public void setPriorityId(final int priority_id) {
        this.priority_id = priority_id;
    }

    public PriorityName getPriorityName() {
        return priority_name;
    }

    public void setPriorityName(final PriorityName priority_name) {
        this.priority_name = priority_name;
    }

    public enum PriorityName {

        LOW("Low"),
        NORMAL("Normal"),
        HIGH("High"),
        URGENT("Urgent"),
        IMMEDIATE("Immediate");

        private String name;

        PriorityName(final String name) {
            this.name = name;
        }

        public static PriorityName value(final String name) {
            if (isBlank(name)) return null;
            for (final PriorityName value : values()) if (value.name.equals(name)) return value;
            return null;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Priority)) return false;
        final Priority priority = (Priority) o;
        return priority_id == priority.getPriorityId()
            && Objects.equals(priority_name, priority.getPriorityName());
    }

    @Override
    public int hashCode() {
        return hash(priority_id, priority_name);
    }
}
