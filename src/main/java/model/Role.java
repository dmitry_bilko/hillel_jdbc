package model;

import java.util.Objects;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import static java.util.Objects.hash;

import static org.apache.commons.lang3.StringUtils.isBlank;

@Entity
@Table
@SuppressWarnings({ "WeakerAccess", "unused" })
public class Role {

    @Id
    private int role_id;

    @Convert(converter = RoleNameConverter.class)
    private RoleName role_name;

    public Role() { }

    public int getRoleId() {
        return role_id;
    }

    public void setRoleId(final int role_id) {
        this.role_id = role_id;
    }

    public RoleName getRoleName() {
        return role_name;
    }

    public void setRoleName(final RoleName role_name) {
        this.role_name = role_name;
    }

    public enum RoleName {

        DEVELOPER("Developer"),
        QA_ENGINEER("QA Engineer"),
        PRODUCT_OWNER("Product Owner");

        private String name;

        RoleName(final String name) {
            this.name = name;
        }

        public static RoleName value(final String name) {
            if (isBlank(name)) return null;
            for (final RoleName value : values()) if (value.name.equals(name)) return value;
            return null;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Role)) return false;
        final Role role = (Role) o;
        return role_id == role.getRoleId() && Objects.equals(role_name, role.getRoleName());
    }

    @Override
    public int hashCode() {
        return hash(role_id, role_name);
    }
}
