package model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import static java.util.Objects.hash;

@Entity
@Table(name = "User")
@SuppressWarnings({ "WeakerAccess", "unused" })
public class User {

    private static final String ROLE_FK = "role_id";

    @Id
    private int user_id;

    @Column(length = 32)
    private String first_name;

    @Column(length = 50)
    private String last_name;

    @JoinColumn(name = ROLE_FK)
    @ManyToOne
    private Role role;

    public User() { }

    public int getUserId() {
        return user_id;
    }

    public void setUserId(final int user_id) {
        this.user_id = user_id;
    }

    public String getFirstName() {
        return first_name;
    }

    public void setFirstName(final String first_name) {
        this.first_name = first_name;
    }

    public String getLastName() {
        return last_name;
    }

    public void setLastName(final String last_name) {
        this.last_name = last_name;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(final Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        final User user = (User) o;
        return user_id == user.getUserId()
            && Objects.equals(first_name, user.getFirstName())
            && Objects.equals(last_name, user.getLastName())
            && Objects.equals(role, user.getRole());
    }

    @Override
    public int hashCode() {
        return hash(user_id, first_name, last_name, role);
    }
}
