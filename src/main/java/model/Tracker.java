package model;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import java.util.Objects;

import static java.util.Objects.hash;

import static org.apache.commons.lang3.StringUtils.isBlank;

@Entity
@Table
@SuppressWarnings({ "WeakerAccess", "unused" })
public class Tracker {

    @Id
    private int tracker_id;

    @Convert(converter = TrackerNameConverter.class)
    private TrackerName tracker_name;

    public Tracker() { }

    public int getTrackerId() {
        return tracker_id;
    }

    public void setTrackerId(final int tracker_id) {
        this.tracker_id = tracker_id;
    }

    public TrackerName getTrackerName() {
        return tracker_name;
    }

    public void setTrackerName(final TrackerName tracker_name) {
        this.tracker_name = tracker_name;
    }

    public enum TrackerName {

        BUG("Bug"),
        FEATURE("Feature"),
        SUPPORT("Support"),
        STORY("Story");

        private String name;

        TrackerName(final String name) {
            this.name = name;
        }

        public static TrackerName value(final String name) {
            if (isBlank(name)) return null;
            for (final TrackerName value : values()) if (value.name.equals(name)) return value;
            return null;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Tracker)) return false;
        final Tracker tracker = (Tracker) o;
        return tracker_id == tracker.getTrackerId()
            && Objects.equals(tracker_name, tracker.getTrackerName());
    }

    @Override
    public int hashCode() {
        return hash(tracker_id, tracker_name);
    }
}
